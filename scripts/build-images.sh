#!/bin/bash

docker build -t dsousasantos91/discovery:1.0.0 ../discovery
docker build -t dsousasantos91/gateway:1.0.0 ../gateway
docker build -t dsousasantos91/ms-notifications-client:1.0.0 ../ms-notifications-client
docker build -t dsousasantos91/ms-notifications-business:1.0.0 ../ms-notifications-business