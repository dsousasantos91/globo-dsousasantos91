#!/bin/bash

filename='../notificacoes.txt'
n=0

responseHealth=""

while [ "$responseHealth" != "{\"status\":\"UP\"}" ]; do
    responseHealth=$( curl -sb -H "Accept: application/json" http://localhost:8080/ms-notifications-client/api/v1/actuator/health 2>&1 )
    clear
    echo "Wait a moment. Health check server..."
    sleep 2
done

echo "Response Health: $responseHealth"
echo "Sending notifications..."

while read line; do
    if [ -n "$line" ]
    then
        object_json=$(echo $line | sed -e "s/'/\"/g")
        curl -d "${object_json}" -H "Content-Type: application/json" -X POST http://localhost:8080/ms-notifications-client/api/v1/notifications/send
        echo ""
        n=$((n+1))
    fi
done < $filename

echo "Total requests sent: ${n}"
sleep 5
clear