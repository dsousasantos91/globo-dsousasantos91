#!/bin/bash

mvn clean install -DskipTests -f ../discovery/
mvn clean install -DskipTests -f ../gateway/
mvn clean install -DskipTests -f ../ms-notifications-client/
mvn clean install -DskipTests -f ../ms-notifications-business/