#!/bin/bash

docker container rm $(docker ps -a -q) -f

docker run -dit -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=pg_client -p 5432:5432 --network globo-network --name postgresql_client postgres:12-alpine
docker run -dit -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=pg_business -p 5433:5432 --network globo-network --name postgresql_business postgres:12-alpine
docker run -dit -e RABBITMQ_ERLANG_COOKIE=secret_pass -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin -p 5672:5672 -p 15672:15672 --network globo-network --name rabbitmq rabbitmq:3.8.3-management 
docker run -dit -e PGADMIN_DEFAULT_EMAIL=admin@admin.com -e PGADMIN_DEFAULT_PASSWORD=root -p 5050:80 --network globo-network --name pgadmin4 dpage/pgadmin4
docker run -dit -p 8761:8761 --network globo-network --name discovery dsousasantos91/discovery:1.0.0
docker run -dit -e SPRING_PROFILES_ACTIVE=prod -p 8080:8080 --network globo-network --name gateway dsousasantos91/gateway:1.0.0
docker run -dit -e SPRING_PROFILES_ACTIVE=prod -P --network globo-network --name ms-notifications-client dsousasantos91/ms-notifications-client:1.0.0
docker run -dit -e SPRING_PROFILES_ACTIVE=prod -P --network globo-network --name ms-notifications-business dsousasantos91/ms-notifications-business:1.0.0