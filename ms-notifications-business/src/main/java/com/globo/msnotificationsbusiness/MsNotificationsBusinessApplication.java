package com.globo.msnotificationsbusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsNotificationsBusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsNotificationsBusinessApplication.class, args);
	}

}
