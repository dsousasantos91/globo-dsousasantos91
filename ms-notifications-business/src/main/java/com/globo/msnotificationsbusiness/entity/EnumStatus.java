package com.globo.msnotificationsbusiness.entity;

public enum EnumStatus {
    ACTIVE(1L),
    CANCELED(2L),
    FAILED(3L);

    private Long value;

    EnumStatus(long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }
}
