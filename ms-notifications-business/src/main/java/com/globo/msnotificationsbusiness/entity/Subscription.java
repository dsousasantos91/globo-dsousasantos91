package com.globo.msnotificationsbusiness.entity;

import com.globo.msnotificationsbusiness.dto.EnumNotificationType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "subscription")
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Subscription {

    @Getter
    @Setter
    @Id
    @Column(name = "id")
    @NotBlank
    private String id;

    @Getter
    @OneToOne
    @JoinColumn(name = "status_id")
    @NotNull
    private Status status;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    public void setStatus(EnumNotificationType notificationType) {
        this.status = EnumNotificationType.SUBSCRIPTION_CANCELED.equals(notificationType) ?
                Status.builder().id(EnumStatus.CANCELED.getValue()).build() :
                Status.builder().id(EnumStatus.ACTIVE.getValue()).build();
    }

    @PrePersist
    private void beforePersist() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    @PreUpdate
    private void beforeUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
}
