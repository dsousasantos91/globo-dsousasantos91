package com.globo.msnotificationsbusiness.service;

import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.entity.EventHistory;
import com.globo.msnotificationsbusiness.entity.Subscription;

public interface EventHistoryService {
    EventHistory saveOrUpdate(Subscription subscription, NotificationDTO notificationDTO);
}
