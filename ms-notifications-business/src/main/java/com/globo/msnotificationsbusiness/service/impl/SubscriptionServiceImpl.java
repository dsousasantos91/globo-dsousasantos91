package com.globo.msnotificationsbusiness.service.impl;

import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.dto.SubscriptionDTO;
import com.globo.msnotificationsbusiness.entity.EnumStatus;
import com.globo.msnotificationsbusiness.entity.Status;
import com.globo.msnotificationsbusiness.entity.Subscription;
import com.globo.msnotificationsbusiness.exception.SubscriptionFailedException;
import com.globo.msnotificationsbusiness.message.SubscriptionSendMessage;
import com.globo.msnotificationsbusiness.repository.SubscriptionRepository;
import com.globo.msnotificationsbusiness.service.SubscriptionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@AllArgsConstructor
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionSendMessage subscriptionSendMessage;
    private final SubscriptionRepository subscriptionRepository;

    @Override
    public Subscription saveOrUpdate(NotificationDTO notificationDTO) {
        Subscription subscription = subscriptionRepository.findById(notificationDTO.getSubscription()).orElse(new Subscription());
        if (isNull(subscription.getId())) {
            subscription.setId(notificationDTO.getSubscription());
        }
        subscription.setStatus(notificationDTO.getNotificationType());

        try {
            Subscription subscriptionSave = subscriptionRepository.save(subscription);
            subscriptionSendMessage.sendMessage(SubscriptionDTO.create(subscriptionSave));
            return subscriptionSave;
        } catch (Exception e) {
            SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder()
                    .status(new Status(EnumStatus.FAILED.getValue(), EnumStatus.FAILED.name()))
                    .build();
            subscriptionSendMessage.sendMessage(subscriptionDTO);
            throw new SubscriptionFailedException("Subscription FAILED");
        }
    }
}
