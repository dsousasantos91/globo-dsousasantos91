package com.globo.msnotificationsbusiness.service;

import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.entity.Subscription;

public interface SubscriptionService {
    Subscription saveOrUpdate(NotificationDTO notificationDTO);
}
