package com.globo.msnotificationsbusiness.service.impl;

import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.entity.EventHistory;
import com.globo.msnotificationsbusiness.entity.Subscription;
import com.globo.msnotificationsbusiness.repository.EventHistoryRepository;
import com.globo.msnotificationsbusiness.service.EventHistoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EventHistoryServiceImpl implements EventHistoryService {

    private final EventHistoryRepository eventHistoryRepository;

    @Override
    public EventHistory saveOrUpdate(Subscription subscription, NotificationDTO notificationDTO) {
        EventHistory eventHistory = EventHistory.builder()
                .notificationType(notificationDTO.getNotificationType())
                .subscription(subscription)
                .build();
        return eventHistoryRepository.save(eventHistory);
    }
}
