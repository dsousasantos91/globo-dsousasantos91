package com.globo.msnotificationsbusiness.repository;

import com.globo.msnotificationsbusiness.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, String> {
}
