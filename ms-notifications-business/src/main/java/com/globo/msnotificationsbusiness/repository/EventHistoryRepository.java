package com.globo.msnotificationsbusiness.repository;

import com.globo.msnotificationsbusiness.entity.EventHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventHistoryRepository extends JpaRepository<EventHistory, Long> {
}
