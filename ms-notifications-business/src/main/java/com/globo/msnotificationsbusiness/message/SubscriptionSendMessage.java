package com.globo.msnotificationsbusiness.message;

import com.globo.msnotificationsbusiness.dto.SubscriptionDTO;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionSendMessage {

    @Value("${spring.rabbitmq.template.exchange}")
    private String exchange;

    @Value("${business.rabbitmq.routingkey}")
    private String routingKey;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public SubscriptionSendMessage(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(SubscriptionDTO subscriptionDTO) {
        rabbitTemplate.convertAndSend(exchange, routingKey, subscriptionDTO);
    }
}
