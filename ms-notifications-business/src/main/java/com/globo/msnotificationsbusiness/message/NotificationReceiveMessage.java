package com.globo.msnotificationsbusiness.message;

import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.entity.Subscription;
import com.globo.msnotificationsbusiness.service.EventHistoryService;
import com.globo.msnotificationsbusiness.service.SubscriptionService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class NotificationReceiveMessage {

    private final SubscriptionService subscriptionService;
    private final EventHistoryService eventHistoryService;

    @RabbitListener(queues = {"${client.rabbitmq.queue}"})
    public void receive(@Payload NotificationDTO notificationDTO) {
        Subscription subscription = subscriptionService.saveOrUpdate(notificationDTO);
        eventHistoryService.saveOrUpdate(subscription, notificationDTO);
    }
}
