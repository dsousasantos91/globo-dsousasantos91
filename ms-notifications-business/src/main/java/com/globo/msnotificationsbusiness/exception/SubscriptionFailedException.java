package com.globo.msnotificationsbusiness.exception;

public class SubscriptionFailedException extends RuntimeException {
    public SubscriptionFailedException(String message) {
        super(message);
    }
}
