package com.globo.msnotificationsbusiness.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class NotificationDTO {

    @JsonProperty("notificationType")
    @JsonAlias({"notificationType", "notification_type"})
    @NotNull
    private EnumNotificationType notificationType;

    @JsonProperty("subscription")
    @JsonAlias("subscription")
    @NotBlank
    private String subscription;
}
