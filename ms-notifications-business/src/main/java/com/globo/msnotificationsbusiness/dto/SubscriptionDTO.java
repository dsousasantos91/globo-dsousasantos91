package com.globo.msnotificationsbusiness.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.globo.msnotificationsbusiness.entity.Status;
import com.globo.msnotificationsbusiness.entity.Subscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class SubscriptionDTO {

    @JsonProperty("subscription")
    @NotBlank
    private String id;

    @JsonProperty("status")
    @NotNull
    private Status status;

    public static SubscriptionDTO create(Subscription subscription) {
        return new ModelMapper().map(subscription, SubscriptionDTO.class);
    }
}
