create table event_history(
    id serial not null,
    type varchar(255),
    subscription_id varchar(255),
    created_at timestamp,

    primary key (id),
    foreign key (subscription_id) references subscription(id)
)