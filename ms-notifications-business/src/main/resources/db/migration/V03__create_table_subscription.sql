create table subscription(
    id varchar(255) not null,
    status_id int,
    created_at timestamp,
    updated_at timestamp,

    primary key (id),
    foreign key (status_id) references status(id)
)