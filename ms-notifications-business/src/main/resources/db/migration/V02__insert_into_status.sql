insert into status (id, name)
values (1, 'ACTIVE'),
       (2, 'CANCELED'),
       (3, 'FAILED');