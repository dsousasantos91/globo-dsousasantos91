package com.globo.msnotificationsbusiness.service;

import com.globo.msnotificationsbusiness.dto.EnumNotificationType;
import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.dto.SubscriptionDTO;
import com.globo.msnotificationsbusiness.entity.Subscription;
import com.globo.msnotificationsbusiness.message.SubscriptionSendMessage;
import com.globo.msnotificationsbusiness.repository.SubscriptionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class SubscriptionServiceTest {

    @MockBean
    private SubscriptionSendMessage subscriptionSendMessage;

    @MockBean
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private SubscriptionService subscriptionService;

    @Test
    public void shouldSaveSubscriptionPurchased() {
        // scenario
        NotificationDTO notificationDTO = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        Subscription subscription = new Subscription();
        subscription.setId(notificationDTO.getSubscription());
        subscription.setStatus(notificationDTO.getNotificationType());

        when(subscriptionRepository.save(subscription)).thenReturn(subscription);
        doNothing().when(subscriptionSendMessage).sendMessage(SubscriptionDTO.create(subscription));

        // actions
        Subscription subscriptionReceive = subscriptionService.saveOrUpdate(notificationDTO);

        //asserts
        Assertions.assertEquals(subscription, subscriptionReceive);
    }

    @Test
    public void shouldUpdateSubscriptionCanceled() {
        // scenario
        NotificationDTO notificationDTO = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_CANCELED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        Subscription subscription = new Subscription();
        subscription.setId(notificationDTO.getSubscription());
        subscription.setStatus(notificationDTO.getNotificationType());

        when(subscriptionRepository.findById(notificationDTO.getSubscription())).thenReturn(Optional.of(subscription));
        when(subscriptionRepository.save(subscription)).thenReturn(subscription);

        // actions
        Subscription subscriptionReceive = subscriptionService.saveOrUpdate(notificationDTO);

        //asserts
        Assertions.assertEquals(subscription.getId(), subscriptionReceive.getId());
    }
}