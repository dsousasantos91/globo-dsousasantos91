package com.globo.msnotificationsbusiness.service;

import com.globo.msnotificationsbusiness.dto.EnumNotificationType;
import com.globo.msnotificationsbusiness.dto.NotificationDTO;
import com.globo.msnotificationsbusiness.entity.EventHistory;
import com.globo.msnotificationsbusiness.entity.Subscription;
import com.globo.msnotificationsbusiness.repository.EventHistoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class EventHistoryServiceTest {

    @MockBean
    private EventHistoryRepository eventHistoryRepository;

    @Autowired
    private EventHistoryService eventHistoryService;

    @Test
    public void shouldSaveEventHistory() {
        // scenario
        NotificationDTO notificationDTO = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        Subscription subscription = new Subscription();
        subscription.setId(notificationDTO.getSubscription());
        subscription.setStatus(notificationDTO.getNotificationType());

        EventHistory eventHistory = new EventHistory();
        eventHistory.setSubscription(subscription);
        eventHistory.setNotificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED);

        when(eventHistoryRepository.save(any(EventHistory.class))).thenReturn(eventHistory);

        // actions
        EventHistory eventHistoryReceive = eventHistoryService.saveOrUpdate(subscription, notificationDTO);

        // asserts
        Assertions.assertEquals(eventHistory, eventHistoryReceive);
    }
}