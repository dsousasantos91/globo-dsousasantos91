drop table if exists `status` cascade;

create table `status`
(
    id   int auto_increment not null,
    name varchar(255)       not null,

    primary key (id)
);

insert into `status` (id, name)
values (1, 'ACTIVE'),
       (2, 'CANCELED'),
       (3, 'FAILED');

drop table if exists subscription cascade;

create table subscription
(
    id         varchar(255) not null,
    status_id  int,
    created_at timestamp,
    updated_at timestamp,

    primary key (id),
    foreign key (status_id) references `status` (id)
);

drop table if exists event_history cascade;

create table event_history
(
    id              int auto_increment not null,
    type            varchar(255),
    subscription_id varchar(255),
    created_at      timestamp,

    primary key (id),
    foreign key (subscription_id) references subscription (id)
);