
# **Desafio NTConsulting/Globo**

## **Índice**
- [Autor](#autor)
- [Apresentação](#apresentação)
- [Arquitetura](#arquitetura)
- [Tecnologias](#tecnologias)
- [Pré-requisitos](#pré-requisitos)
- [Execução](#execução)
  - [Postman](#postman)
- [RabbitMQ](#rabbitmq)
  - [Exchanges](#exchanges)
  - [Queues](#queues)
- [PGAdmin](#pgadmin)
- [Swagger-UI](#swagger-ui)
- [Testes executados](#testes-executados)
- [Ideias para implementação](#ideias-para-implementação)

## **Autor**
--- 
## Douglas de Sousa Santos
- [<img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" />](dsousasantos91@gmail.com) 
- [<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />](https://www.linkedin.com/in/dsousasantos91/)
- [<img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white" />](https://github.com/dsousasantos91)


## **Apresentação**
---

Arquitetura criada para atender ao requisitos de um serviço de assinaturas tratandos as requisições de forma assíncronas.

Existem 3 tipos de ações que podem ser executadas em uma assinatura. São elas: 

-  SUBSCRIPTION_PURCHASED - A Compra foi realizada e a assinatura deve estar com status ativa.

-  SUBSCRIPTION_CANCELED - A Compra foi cancelada e a assinatura deve estar com status cancelada.

-  SUBSCRIPTION_RESTARTED - A Compra foi recuperada e a assinatura deve estar com status ativa.

Nesta API foram implementadas as funções de enviar uma notificação com o tipo de ação a ser executada sob um assinatura, obter a lista de assinatura registradas e seu status com e sem paginação e buscar uma assinatura por sua identificação.


## **Arquitetura**
---
O projeto é composto por 7 serviços distribuidos e isolados em conteiners Docker. São eles:

- GATEWAY
- DISCOVERY
- MS-NOTIFICATIONS-CLIENT
- MS-NOTIFICATIONS-BUSINESS
- MESSAGE BROKER (RABBITMQ)
- POSTGRESQL_CLIENT (DATABASE MS-NOTIFICATIONS-CLIENT)
- POSTGRESQL_BUSINESS (DATABASE MS-NOTIFICATIONS-BUSINESS)

## **Tecnologias**
---
Para implementar o projeto foram utilizadas as seguintes tecnologias:

- Java 11
- Spring (Boot, Web, Cloud, Data, Test)
- SpringFox (Swagger2)
- JUnit 5
- Maven
- H2
- Flyway
- PostgreSQL
- RabbiMQ
- Docker

## **Pré-requisitos**
---
- Linux (Bash, Curl)
- Docker
- Postman *(Opcional)*

## **Execução**
---
Para execurtar toda a arquitetura do projeto é necessário o Docker e Docker-compose. 

Execute o comando abaixo dentro do diretório raiz do projeto para carregar o serviço com `docker-compose`:

```
docker-compose up -d
```

Na falta do Docker-compose o processe de construção e execução dos projetos e imagens podem ser carregados pelos script `build-run-all.sh` implementados dentro da pasta `scripts`.

*OBS.: Todos os scripts devem ser executados dentro da própria pasta `scripts`.*

```
cd scripts

./build-projects.sh
./build-images.sh
./run-containers.sh

ou 

./build-run-all.sh
```

Após subir os conteiners *(seja por docker-compose ou scripts bash)*, pode-se navegar imediatamente até o diretório `scripts` e chamar o código que ler o arquivo `notificacoes.txt` na pasta raiz do projeto e executa as requisições no serviço.

```
cd scripts

./send_notifications.sh
```

### **Postman**

Outra opção de execução, é importar o arquivo `Notificacoes.postman_collection.json`, usar a requisição de `Health Check` para verificar o estado do serviço e a requisição `Enviar Notificações em Looping` para enviar as mesmas notificações contidas no arquivo `notificacoes.txt`.

![postman_requests](images/postman_requests.png)

## **RabbitMQ**
---
- [Link](http://localhost:15672/#/)
- **User:** admin
- **Password:** admin

### **Exchanges** 
- *direct-exchange*
![rabbit_exchanges](images/rabbit_exchanges.png)

### **Queues**
- *client.queue*
- *business.queue*
![rabbit_queues](images/rabbit_queues.png)

## **PGAdmin**
---
- [Link](http://localhost:5050/)
- **User:** admin@admin.com
- **Password:** root

#### Para acessar as bases de dados deve-se criar as 2 conexões a seguir:

- *PG_CLIENT*
  - **User:** postgres
  - **Password:** postgres

![pg_client_create-server_general](images/pgadmin_business_create-server_general.png)
![pg_client_create-server_connection](images/pgadmin_client_create-server_connection.png)


- *PG_BUSINES*
  - **User:** postgres
  - **Password:** postgres

![pg_client_create-server_general](images/pgadmin_client_create-server_general.png)
![pg_client_create-server_connection](images/pgadmin_business_create-server_connection.png)


## **Swagger-UI**
---
- [Link](http://localhost:8080/ms-notifications-client/api/v1/swagger-ui/index.html?configUrl=/api/v1/api-docs)

![swagger_operation](images/swagger_operation.png)
![swagger_model](images/swagger_models.png)


# Observações

## **Testes executados**

Além dos teste funcionais através da ferramenta `Postman` para garantir que os requisitos foram atendidos *(comprar, cancelar e reativar uma assinatura)* também foi realizado testes como:

- Garantir que uma assinatura com status `ACTIVE` não receba uma nova requisição do tipo `SUBSCRIPTION_PURCHASED` ou `SUBSCRIPTION_RESTARTED`, assim como uma coM status `CANCELED` não receba uma assinatura com tipo `SUBSCRIPTION_CANCELED`.

- Não permitir que uma assinatura comprada e cancelada anteriormente receba uma requisição com tipo de assinatura `SUBSCRIPTION_PURCHASED`. Se a assinatura foi comprada e cancelada agora para ativá-la o tipo de notificação deve `SUBSCRIPTION_RESTARTED`.

## **Ideias para implementação**

- Com tempo hábil seriam implementados testes de integração usando as bibliotecas Cucumber e RestAssured em um projeto apartado.
- Gostaria de implementar um serviço de e-mail para notificar o cliente sobre sua assinaturas e possíveis falhas durante o processo.
- Criar uma Pipeline de CI/CD com Jenkins.