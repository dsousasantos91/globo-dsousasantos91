create table status
(
    id   serial primary key not null,
    name varchar(255)       not null
);

insert into status (id, name)
values (1, 'ACTIVE'),
       (2, 'CANCELED'),
       (3, 'FAILED'),
       (4, 'IN_PROGRESS');