create table status_notifications
(
    subscription varchar(255) not null,
    status_id    int          not null,

    primary key (subscription)
);