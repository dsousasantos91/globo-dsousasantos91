package com.globo.msnotificationsclient.exceptions;

public class RabbitConnectionRefusedException extends RuntimeException {
    public RabbitConnectionRefusedException(String message) {
        super(message);
    }
}
