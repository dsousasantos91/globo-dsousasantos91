package com.globo.msnotificationsclient.exceptions;

public class NotificationTypeNotFoundException extends RuntimeException {
    public NotificationTypeNotFoundException(String message) {
        super(message);
    }
}
