package com.globo.msnotificationsclient.repository;

import com.globo.msnotificationsclient.entity.StatusNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusNotificationRepository extends JpaRepository<StatusNotification, String> {
}
