package com.globo.msnotificationsclient.controller;

import com.globo.msnotificationsclient.dto.NotificationDTO;
import com.globo.msnotificationsclient.dto.StatusNotificationDTO;
import com.globo.msnotificationsclient.service.NotificationService;
import com.globo.msnotificationsclient.service.StatusNotificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "notifications")
@AllArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;
    private final StatusNotificationService statusNotificationService;

    @ApiOperation(value = "Send notifications")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Send notification"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping(path = "send", produces = {"application/json", "application/xml", "application/x-yaml"},
            consumes = {"application/json", "application/xml", "application/x-yaml"})
    public ResponseEntity<Map<String, String>> sendNotification(@Valid @RequestBody NotificationDTO notificationDTO) {
        notificationService.sendNotification(notificationDTO);
        return ResponseEntity.ok(Map.of("message", "Subscription " + notificationDTO.getSubscription() + " sent successfully"));
    }

    @ApiOperation(value = "Find all notifications with status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Send notification"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(path = "all", produces = {"application/json", "application/xml", "application/x-yaml"})
    public ResponseEntity<List<StatusNotificationDTO>> findAll() {
        List<StatusNotificationDTO> statusNotificationDTOList = statusNotificationService.findAll();
        return ResponseEntity.ok(statusNotificationDTOList);
    }

    @ApiOperation(value = "Find all notifications with paginations")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Send notification"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(path = "page", produces = {"application/json", "application/xml", "application/x-yaml"})
    public ResponseEntity<Page<StatusNotificationDTO>> findAllPagination(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "limit", defaultValue = "12") int limit,
            @RequestParam(value = "sorted", defaultValue = "asc") String direction
    ) {
        Direction directionSend = "desc".equalsIgnoreCase(direction) ? Direction.DESC : Direction.ASC;

        Pageable pageable = PageRequest.of(page, limit, Sort.by(directionSend, "subscription"));
        Page<StatusNotificationDTO> statusNotificationDTOList = statusNotificationService.findAllPatination(pageable);

        return ResponseEntity.ok(statusNotificationDTOList);
    }

    @ApiOperation(value = "Find notifications and status by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Send notification"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(path = "{id}", produces = {"application/json", "application/xml", "application/x-yaml"})
    public ResponseEntity<StatusNotificationDTO> findById(@PathVariable("id") String subscription) {
        StatusNotificationDTO statusNotificationDTO = statusNotificationService.findById(subscription);
        return ResponseEntity.ok(statusNotificationDTO);
    }
}
