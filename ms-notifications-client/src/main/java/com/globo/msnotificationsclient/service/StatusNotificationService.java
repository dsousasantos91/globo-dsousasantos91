package com.globo.msnotificationsclient.service;

import com.globo.msnotificationsclient.dto.StatusNotificationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StatusNotificationService {

    Page<StatusNotificationDTO> findAllPatination(Pageable pageable);

    List<StatusNotificationDTO> findAll();

    StatusNotificationDTO findById(String s);
}
