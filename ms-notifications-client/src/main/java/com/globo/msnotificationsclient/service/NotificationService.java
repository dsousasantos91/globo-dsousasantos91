package com.globo.msnotificationsclient.service;

import com.globo.msnotificationsclient.dto.NotificationDTO;

public interface NotificationService {

    void sendNotification(NotificationDTO notification);
}
