package com.globo.msnotificationsclient.service.impl;

import com.globo.msnotificationsclient.dto.StatusNotificationDTO;
import com.globo.msnotificationsclient.entity.StatusNotification;
import com.globo.msnotificationsclient.exceptions.ResourceNotFoundException;
import com.globo.msnotificationsclient.repository.StatusNotificationRepository;
import com.globo.msnotificationsclient.service.StatusNotificationService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class StatusNotificationServiceImpl implements StatusNotificationService {

    private final StatusNotificationRepository statusNotificationRepository;

    @Override
    public Page<StatusNotificationDTO> findAllPatination(Pageable pageable) {
        Page<StatusNotification> page = statusNotificationRepository.findAll(pageable);
        return page.map(StatusNotificationDTO::create);
    }

    @Override
    public List<StatusNotificationDTO> findAll() {
        return statusNotificationRepository.findAll().stream().map(StatusNotificationDTO::create).collect(toList());
    }

    @Override
    public StatusNotificationDTO findById(String subscription) {
        StatusNotification statusNotification = statusNotificationRepository.findById(subscription)
                .orElseThrow(() -> new ResourceNotFoundException("Resource NOT FOUND"));
        return StatusNotificationDTO.create(statusNotification);
    }
}
