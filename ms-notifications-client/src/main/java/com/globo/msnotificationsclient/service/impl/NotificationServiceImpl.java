package com.globo.msnotificationsclient.service.impl;

import com.globo.msnotificationsclient.dto.EnumNotificationType;
import com.globo.msnotificationsclient.dto.NotificationDTO;
import com.globo.msnotificationsclient.entity.EnumStatus;
import com.globo.msnotificationsclient.entity.Status;
import com.globo.msnotificationsclient.entity.StatusNotification;
import com.globo.msnotificationsclient.exceptions.NotificationTypeNotFoundException;
import com.globo.msnotificationsclient.exceptions.RabbitConnectionRefusedException;
import com.globo.msnotificationsclient.exceptions.ExistingResourceException;
import com.globo.msnotificationsclient.message.NotificationSendMessage;
import com.globo.msnotificationsclient.repository.StatusNotificationRepository;
import com.globo.msnotificationsclient.service.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.AmqpConnectException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private final NotificationSendMessage notificationSendMessage;
    private final StatusNotificationRepository statusNotificationRepository;

    @Override
    public void sendNotification(NotificationDTO notificationDTO) {

        boolean notificationTypeExists = List.of(EnumNotificationType.values()).contains(notificationDTO.getNotificationType());
        if (!notificationTypeExists) {
            throw new NotificationTypeNotFoundException("Notification Type " + notificationDTO.getNotificationType() + " not exist.");
        }

        Optional<StatusNotification> statusNotificationById = statusNotificationRepository.findById(notificationDTO.getSubscription());
        statusNotificationById.ifPresent(
                statusNotification -> checkSubscriptionStatus(statusNotification.getStatus(), notificationDTO.getNotificationType())
        );

        try {
            notificationSendMessage.sendMessage(notificationDTO);
            StatusNotification statusNotification = StatusNotification.builder()
                    .status(new Status(EnumStatus.IN_PROGRESS.getValue(), EnumStatus.IN_PROGRESS.name()))
                    .subscription(notificationDTO.getSubscription())
                    .build();
            statusNotificationRepository.save(statusNotification);
        } catch (AmqpConnectException ex) {
            throw new RabbitConnectionRefusedException("Rabbit connection refused.");
        }
    }

    private void checkSubscriptionStatus(Status status, EnumNotificationType notificationType) {
        if (EnumStatus.CANCELED.name().equals(status.getName()) && EnumNotificationType.SUBSCRIPTION_CANCELED.equals(notificationType)) {
            throw new ExistingResourceException("Subscription is CANCELED");
        }

        if (EnumStatus.ACTIVE.name().equals(status.getName()) &&
                (EnumNotificationType.SUBSCRIPTION_PURCHASED.equals(notificationType) || EnumNotificationType.SUBSCRIPTION_RESTARTED.equals(notificationType))) {
            throw new ExistingResourceException("Subscription is ACTIVE");
        }

        if (EnumStatus.CANCELED.name().equals(status.getName()) && EnumNotificationType.SUBSCRIPTION_PURCHASED.equals(notificationType)) {
            throw new ExistingResourceException("Subscription exists and must be RESTARTED");
        }
    }
}
