package com.globo.msnotificationsclient.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfig {

    @Value("${client.rabbitmq.queue}")
    String clientQueue;

    @Value("${business.rabbitmq.queue}")
    String businessQueue;

    @Value("${spring.rabbitmq.template.exchange}")
    String exchange;

    @Value("${client.rabbitmq.routingkey}")
    private String clientRoutingkey;

    @Value("${business.rabbitmq.routingkey}")
    private String businessRoutingkey;

    @Bean
    public Queue declareClientQueue() {
        return QueueBuilder.durable(clientQueue).build();
    }

    @Bean
    public Queue declareBusinessQueue() {
        return QueueBuilder.durable(businessQueue).build();
    }

    @Bean
    public DirectExchange declareExchange() {
        return ExchangeBuilder.directExchange(exchange).durable(true).build();
    }

    @Bean
    public Binding declareClientBinding(@Qualifier("declareClientQueue") Queue notificationsQueue, DirectExchange exchange) {
        return BindingBuilder.bind(notificationsQueue).to(exchange).with(clientRoutingkey);
    }

    @Bean
    public Binding declareSubscriptionsBinding(@Qualifier("declareBusinessQueue") Queue subscriptionsQueue, DirectExchange exchange) {
        return BindingBuilder.bind(subscriptionsQueue).to(exchange).with(businessRoutingkey);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
