package com.globo.msnotificationsclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
@Import({BeanValidatorPluginsConfiguration.class})
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.globo.msnotificationsclient.controller"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .globalResponses(HttpMethod.GET, responseForGET())
                .globalResponses(HttpMethod.POST, responseForPOST())
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Microservice Notifications")
                .description("API to buy, cancel and restart subscription")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                        .contact(new Contact("Douglas Santos", "https://github.com/dsousasantos91", "dsousasantos91@gmail.com"))
                        .build();
    }

    private List<Response> responseForGET() {
        return List.of(
                new ResponseBuilder().code("200").description("OK").build(),
                new ResponseBuilder().code("400").description("Bad Request").build(),
                new ResponseBuilder().code("404").description("Not Found").build(),
                new ResponseBuilder().code("500").description("Internal server error").build()
        );
    }

    private List<Response> responseForPOST() {
        return List.of(
                new ResponseBuilder().code("200").description("OK").build(),
                new ResponseBuilder().code("400").description("Bad Request").build(),
                new ResponseBuilder().code("500").description("Internal server error").build()
        );
    }
}
