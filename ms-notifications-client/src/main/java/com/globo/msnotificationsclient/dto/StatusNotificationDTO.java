package com.globo.msnotificationsclient.dto;

import com.globo.msnotificationsclient.entity.EnumStatus;
import com.globo.msnotificationsclient.entity.StatusNotification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class StatusNotificationDTO {

    private String subscription;

    private EnumStatus status;

    public static StatusNotificationDTO create(StatusNotification statusNotification) {
        return StatusNotificationDTO.builder()
                .subscription(statusNotification.getSubscription())
                .status(EnumStatus.valueOf(statusNotification.getStatus().getName()))
                .build();
    }
}
