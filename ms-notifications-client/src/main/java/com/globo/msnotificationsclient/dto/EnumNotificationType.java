package com.globo.msnotificationsclient.dto;

public enum EnumNotificationType {

    SUBSCRIPTION_PURCHASED,
    SUBSCRIPTION_CANCELED,
    SUBSCRIPTION_RESTARTED
}
