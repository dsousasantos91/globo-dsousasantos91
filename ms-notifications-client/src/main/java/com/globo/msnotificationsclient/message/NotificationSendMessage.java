package com.globo.msnotificationsclient.message;

import com.globo.msnotificationsclient.dto.NotificationDTO;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NotificationSendMessage {

    @Value("${spring.rabbitmq.template.exchange}")
    private String exchange;

    @Value("${client.rabbitmq.routingkey}")
    private String routingKey;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public NotificationSendMessage(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(NotificationDTO notification) {
        rabbitTemplate.convertAndSend(exchange, routingKey, notification);
    }
}
