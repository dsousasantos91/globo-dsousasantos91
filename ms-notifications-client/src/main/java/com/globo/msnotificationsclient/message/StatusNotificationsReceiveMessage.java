package com.globo.msnotificationsclient.message;

import com.globo.msnotificationsclient.entity.StatusNotification;
import com.globo.msnotificationsclient.repository.StatusNotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StatusNotificationsReceiveMessage {

    private final StatusNotificationRepository statusNotificationRepository;

    @RabbitListener(queues = {"${business.rabbitmq.queue}"})
    public void receive(@Payload StatusNotification statusNotification) {
        statusNotificationRepository.save(statusNotification);
    }
}
