package com.globo.msnotificationsclient.entity;

public enum EnumStatus {
    ACTIVE(1L),
    CANCELED(2L),
    FAILED(3L),
    IN_PROGRESS(4L);

    private Long value;

    EnumStatus(long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }
}
