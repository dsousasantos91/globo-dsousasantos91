package com.globo.msnotificationsclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MsNotificationsClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsNotificationsClientApplication.class, args);
	}

}
