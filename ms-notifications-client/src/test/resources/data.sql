drop table if exists `status` cascade;

create table `status`
(
    id   int auto_increment not null,
    name varchar(255)       not null,

    primary key (id)
);

insert into `status`(`name`)
values ('ACTIVE'),
       ('CANCELED'),
       ('FAILED'),
       ('IN_PROGRESS');

drop table if exists status_notifications cascade;

create table status_notifications
(
    subscription      varchar(255) not null,
    status_id         int not null,

    primary key (subscription)
);