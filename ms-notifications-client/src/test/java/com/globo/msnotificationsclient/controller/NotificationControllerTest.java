package com.globo.msnotificationsclient.controller;

import com.globo.msnotificationsclient.dto.EnumNotificationType;
import com.globo.msnotificationsclient.dto.NotificationDTO;
import com.globo.msnotificationsclient.service.NotificationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
class NotificationControllerTest {

    @MockBean
    private NotificationService notificationService;

    @Autowired
    private NotificationController notificationController;

    @Test
    public void shouldSendNotificationSubscriptionPurchasedRequest() {
        // scenario
        NotificationDTO notification = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED)
                .subscription("5793cf6b3fd833521db8c420955e6f01")
                .build();

        doNothing().when(notificationService).sendNotification(notification);

        // actions
        ResponseEntity<Map<String, String>> sendNotificationRequest = notificationController.sendNotification(notification);

        // asserts
        Assertions.assertEquals(Map.of("message", "Subscription purchased successfully"), sendNotificationRequest.getBody());
    }
}