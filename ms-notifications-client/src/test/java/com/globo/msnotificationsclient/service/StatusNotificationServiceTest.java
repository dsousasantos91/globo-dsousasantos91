package com.globo.msnotificationsclient.service;

import com.globo.msnotificationsclient.dto.StatusNotificationDTO;
import com.globo.msnotificationsclient.entity.EnumStatus;
import com.globo.msnotificationsclient.entity.Status;
import com.globo.msnotificationsclient.entity.StatusNotification;
import com.globo.msnotificationsclient.repository.StatusNotificationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class StatusNotificationServiceTest {

    @MockBean
    public StatusNotificationRepository statusNotificationRepository;

    @Autowired
    private StatusNotificationService statusNotificationService;

    @Test
    public void shouldReturnAllStatusNotifications() {
        // scenario
        when(statusNotificationRepository.findAll()).thenReturn(mockStatusNotificationList());
        List<StatusNotificationDTO> statusNotificationDTOList = mockStatusNotificationList().stream().map(StatusNotificationDTO::create).collect(toList());

        // actions
        List<StatusNotificationDTO> statusNotificationDTOListReceveid = statusNotificationService.findAll();

        // asserts
        assertEquals(statusNotificationDTOList, statusNotificationDTOListReceveid);
    }

    @Test
    public void shouldReturnAllStatusNotificationsPageable() {
        // scenario
        int totalPagesExpected = 2;
        Pageable pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "subscription"));

        when(statusNotificationRepository.findAll(pageable)).thenReturn(mockStatusNotificationListPageable(pageable));

        // actions
        Page<StatusNotificationDTO> subscriptionPageable = statusNotificationService.findAllPatination(pageable);

        // asserts
        assertEquals(totalPagesExpected, subscriptionPageable.getTotalPages());
    }

    @Test
    public void shouldReturnByIdStatusNotifications() {
        // scenario
        when(statusNotificationRepository.findById(any(String.class))).thenReturn(Optional.ofNullable(mockStatusNotificationList().get(0)));

        // actions
        StatusNotificationDTO statusNotificationDTO = statusNotificationService.findById("5793cf6b3fd833521db8c420955e6f77");

        // asserts
        StatusNotificationDTO statusNotificationDTOExpected = StatusNotificationDTO.create(mockStatusNotificationList().get(0));
        assertEquals(statusNotificationDTOExpected, statusNotificationDTO);
    }

    private List<StatusNotification> mockStatusNotificationList() {
        return List.of(
                new StatusNotification("5793cf6b3fd833521db8c420955e6f77",
                        new Status(EnumStatus.ACTIVE.getValue(), EnumStatus.ACTIVE.name())),
                new StatusNotification("5793cf6b3fd833521db8c420955e6f88",
                        new Status(EnumStatus.CANCELED.getValue(), EnumStatus.CANCELED.name())),
                new StatusNotification("5793cf6b3fd833521db8c420955e6f99",
                        new Status(EnumStatus.IN_PROGRESS.getValue(), EnumStatus.IN_PROGRESS.name()))
        );
    }

    private PageImpl<StatusNotification> mockStatusNotificationListPageable(Pageable pageable) {
        return new PageImpl<>(mockStatusNotificationList(), pageable, 3);
    }
}