package com.globo.msnotificationsclient.service;

import com.globo.msnotificationsclient.dto.EnumNotificationType;
import com.globo.msnotificationsclient.dto.NotificationDTO;
import com.globo.msnotificationsclient.entity.EnumStatus;
import com.globo.msnotificationsclient.entity.Status;
import com.globo.msnotificationsclient.entity.StatusNotification;
import com.globo.msnotificationsclient.exceptions.ExistingResourceException;
import com.globo.msnotificationsclient.message.NotificationSendMessage;
import com.globo.msnotificationsclient.repository.StatusNotificationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class NotificationServiceTest {

    @MockBean
    private NotificationSendMessage notificationSendMessage;

    @MockBean
    private StatusNotificationRepository statusNotificationRepository;

    @Autowired
    private NotificationService notificationService;

    @Test
    public void shouldSendNotificationSubscriptionPurchased() {
        // scenario
        NotificationDTO notification = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        doNothing().when(notificationSendMessage).sendMessage(notification);

        // actions
        notificationService.sendNotification(notification);

        // asserts
        verify(notificationService, times(1)).sendNotification(notification);
    }

    @Test
    public void shouldThrowsExistingResourceExceptionSubscriptionActive() {
        // scenario
        NotificationDTO notification = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        StatusNotification statusNotification = StatusNotification.builder()
                .subscription(notification.getSubscription())
                .status(new Status(EnumStatus.ACTIVE.getValue(), EnumStatus.ACTIVE.name()))
                .build();

        doNothing().when(notificationSendMessage).sendMessage(notification);
        when(statusNotificationRepository.findById(any(String.class))).thenReturn(Optional.of(statusNotification));

        // actions
        ExistingResourceException exception = assertThrows(ExistingResourceException.class, () -> notificationService.sendNotification(notification));

        // asserts
        assertEquals("Subscription is ACTIVE", exception.getMessage());
    }

    @Test
    public void shouldThrowsExistingResourceExceptionSubscriptionCanceled() {
        // scenario
        NotificationDTO notification = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_CANCELED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        StatusNotification statusNotification = StatusNotification.builder()
                .subscription(notification.getSubscription())
                .status(new Status(EnumStatus.CANCELED.getValue(), EnumStatus.CANCELED.name()))
                .build();

        doNothing().when(notificationSendMessage).sendMessage(notification);
        when(statusNotificationRepository.findById(any(String.class))).thenReturn(Optional.of(statusNotification));

        // actions
        ExistingResourceException exception = assertThrows(ExistingResourceException.class, () -> notificationService.sendNotification(notification));

        // asserts
        assertEquals("Subscription is ACTIVE", exception.getMessage());
    }

    @Test
    public void shouldThrowsExistingResourceExceptionSubscriptionCanceledReceivedPurchasedNotificationType() {
        // scenario
        NotificationDTO notification = NotificationDTO.builder()
                .notificationType(EnumNotificationType.SUBSCRIPTION_PURCHASED)
                .subscription("5793cf6b3fd833521db8c420955e6f99")
                .build();

        StatusNotification statusNotification = StatusNotification.builder()
                .subscription(notification.getSubscription())
                .status(new Status(EnumStatus.CANCELED.getValue(), EnumStatus.CANCELED.name()))
                .build();

        doNothing().when(notificationSendMessage).sendMessage(notification);
        when(statusNotificationRepository.findById(any(String.class))).thenReturn(Optional.of(statusNotification));

        // actions
        ExistingResourceException exception = assertThrows(ExistingResourceException.class, () -> notificationService.sendNotification(notification));

        // asserts
        assertEquals("Subscription exists and must be RESTARTED", exception.getMessage());
    }
}